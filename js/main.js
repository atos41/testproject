

(() =>{
	const hamburgerBtn = document.querySelector(".hamburger-btn"),
	navMenu = document.querySelector(".nav-menu"),
	closeNavBtn = navMenu.querySelector(".close-nav-menu");


	hamburgerBtn.addEventListener("click", showNavMenu);
	closeNavBtn.addEventListener("click", hideNavMenu);

	function showNavMenu(){
		navMenu.classList.add("open");
		// bodyScrollingToggle();
	} 
	function hideNavMenu(){
		navMenu.classList.remove("open");
		fadeOutEffect();
		// bodyScrollingToggle();
	} 
	function fadeOutEffect(){
		document.querySelector(".fade-out-effect").classList.add("active");
		setTimeout(() =>{
			document.querySelector(".fade-out-effect").classList.remove("active");

		},300)
	}
	// attach an event handlern to document
	document.addEventListener("click", ( event) =>{
		if(event.target.classList.contains('link-item')){
			/* make sure event.target.hash has a value before overridding deafult behavior*/ 
			if(event.target.hash !==""){
				// prevent default anchor click behavior
				event.preventDefault();
				const hash = event.target.hash;
				// deactive exisiting active section
				document.querySelector(".section.active").classList.add("hide");
				document.querySelector(".section.active").classList.remove("active");
				// activate new section
				document.querySelector(hash).classList.add("active");
				document.querySelector(hash).classList.remove("hide");
			}

		}
	})

})();



/*---------------about section tabs------------------*/

(() =>{
		const aboutSection = document.querySelector(".about-section"),
		tabsContainer = document.querySelector(".about-tab");

		tabsContainer.addEventListener("click", (event) =>{
			/*----if event.target contains 'tab-item' class and not contain 'active' class */ 
			if(event.target.classList.contains("tab-item")&&
				!event.target.classList.contains("active")){
				const target = event.target.getAttribute("data-target");
				// deactivate existing active'tab-item'
				tabsContainer.querySelector(".active").classList.remove("outer-shadow", "active");
				//activate new 'tab-item'
				event.target.classList.add("active","outer-shadow");
				// deactivate existing active 'tab-content'
				aboutSection.querySelector(".tab-content.active").classList.remove("active");
				//activate new 'tab-content'
				aboutSection.querySelector(target).classList.add("active");
			}
		})
})();

/*------------------Hide all sections except active-------------------- */ 




